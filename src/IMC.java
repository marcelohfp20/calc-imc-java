
import javax.swing.JOptionPane;
public class IMC {
	


	/**
	 * 
	 * @author Marcelo Henrique
	 *
	 */

	
		public static void main(String[] args) {

		
			
			
 
			//VARIAVEL ALTURA E PESO DECLARADA COMO DOUBLE
			double altura, peso;
			
			//DO REPETI��O
			do {

				
				
				// SOLICITA AO USUARIO QUE INFORME SEU PESO
				peso = JOptionPane.showInputDialog("Informe seu peso", "SENAI - SP", JOptionPane.PLAIN_MESSAGE));

				// SOLICITA QUE O USUARIO INFORME SUA ALTURA
			    altura = JOptionPane.showInputDialog("Informe a temperatura", "SENAI - SP", JOptionPane.PLAIN_MESSAGE));

				System.out.println("Unidade: " + unidade);
				System.out.println("Unidade: " + temp);

				// CONVERTE DE STRING PARA DOUBLE
				temperatura = Double.parseDouble(temp);

				System.out.println(temperatura);

				String msg;

				// CALCULAR NOVAMENTE

				switch (unidade) {
				case "C":
				case "c":
				case "Celsius":
				case "celsius":
					System.out.println("Celsius");

					// REALIZA CONFVERS�O PARA FAHRENHEIT
					fahrenheit = (temperatura * 1.8) + 32;

					// REALIZA CONVERS�O PARA KELVIN
					kelvin = temperatura + 273.15;

					// MENSAGEM DE SAIDA
					msg = fahrenheit + "�F\n" + kelvin + "�K";
					JOptionPane.showMessageDialog(null, msg);
					break;
				case "F":
				case "f":
				case "Fahrenheit":
				case "fahrenheit":

					// REALIZA CONVERS�O PARA CELSIUS
					celsius = (temperatura - 32) / 1.8;
					// REALIZA CONVERS�O PARA KELVIN
					kelvin = (temperatura + 459.67) * 0.5555555;

					msg = celsius + "�C\n" + kelvin + " �K";
					JOptionPane.showMessageDialog(null, msg);
					break;
				case "K":
				case "k":
				case "Kelvin":
				case "kelvin":

					// REALIZA CONVERS�O PARA CELSIUS
					celsius = temperatura - 273.15;
					// REALIZA CONVERS�O PARA FAHRENHEIT
					fahrenheit = temperatura * 1.8 - 459.67;

					msg = celsius + " �C\n" + fahrenheit + " �F";
					JOptionPane.showMessageDialog(null, msg);
					break;
				default:
					System.out.println("Unidade Invalida");
					break;
				}// FIM DO SWITCH CASE
					// VERIFICA SE O USUARIO DESEJA UM NOVO CALCULO
					// 0 = sim
					// 1 = nao
					// 2 = cancelar
				novamente = JOptionPane.showConfirmDialog(null, "Deseja calcular novamente?", "pergunta...",
						JOptionPane.YES_NO_OPTION);
			} while (novamente == 0);
		}// FIM DO METODO MAIN

	}// FIM DA CLASSE

}
