
import javax.swing.JOptionPane;

public class CalcIMC {
	/*
	 * 
	 * Produced by MARCELO HENRIQUE�
	 */
	public static void main(String[] args) {

		// DECLARA A VARIAVEL NOVAMENTE PARA O USARIO REPETIR O CALCULO
		int novamente;
		// DO REPETI��O
		do {

			// SOLICITA AO USUARIO QUE INFORME SEU PESO
			// DECLARA STRING
			String peso = JOptionPane.showInputDialog(null, "Informe seu peso\n KG", "SENAI",
					JOptionPane.INFORMATION_MESSAGE);
			// CONVERTER STRING PARA DOUBLE; PARA O SHOWINPUTDIALOG D� CERTO
			double gordura = Double.parseDouble(peso);
			// SOLICITA QUE O USUARIO INFORME SUA ALTURA
			String altura = JOptionPane.showInputDialog(null, "Informe sua altura\n METROS", "SENAI",
					JOptionPane.INFORMATION_MESSAGE);
			// CONVERTE STRING PARA DOUBLE; PARA SHOWINPUT D� CERTO
			double metros = Double.parseDouble(altura);

			// DECLARA A VARIAVEL IMC
			Double imc;
			// CALCULO IMC
			imc = gordura / (metros * metros);

			// "SE IMC FOR TAL... FA�A" - AO CONTRARIO FA�A...
			if (imc < 18.5) {
				JOptionPane.showMessageDialog(null, "ABAIXO DO PESO\n" + imc, "SENAI �",
						JOptionPane.INFORMATION_MESSAGE);
			} else if (imc >= 18.5 && imc < 25) {
				JOptionPane.showMessageDialog(null, "PESO NORMAL\n" + imc, "SENAI �", JOptionPane.INFORMATION_MESSAGE);
			} else if (imc >= 25 && imc < 30) {
				JOptionPane.showMessageDialog(null, "EXCESSO DE PESO\n" + imc, "SENAI �",
						JOptionPane.INFORMATION_MESSAGE);
			} else if (imc >= 30 && imc < 35) {
				JOptionPane.showMessageDialog(null, "OBESIDADE CLASSE I\n" + imc, "SENAI �",
						JOptionPane.INFORMATION_MESSAGE);
			} else if (imc >= 35 && imc < 39.9) {
				JOptionPane.showMessageDialog(null, "OBESIDADE CLASSE II\n" + imc, "SENAI �",
						JOptionPane.INFORMATION_MESSAGE);
			} else if (imc >= 40) {
				JOptionPane.showMessageDialog(null, "OBESIDADE CLASSE III\n" + imc, "SENAI �",
						JOptionPane.INFORMATION_MESSAGE);

			}
			// CAIXA DE "VOC� DESEJA FAZER UM NOVO CALCULO"
			novamente = JOptionPane.showConfirmDialog(null, "Deseja calcular novamente?", "SENAI �",
					JOptionPane.YES_NO_OPTION);
			// WHILE = FIM DO "DO" OU SEJA FIM DO LA�O DE REPTIC�O...
		} while (novamente == 0);

	}// FIM DO MAIN

}// FIM DO CLASS
